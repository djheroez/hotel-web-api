package ni.edu.ucem.webapi.dao;

import java.util.Date;
import java.util.Optional;

import ni.edu.ucem.webapi.modelo.Cupo;
import ni.edu.ucem.webapi.modelo.Huesped;
import ni.edu.ucem.webapi.modelo.Reservacion;

public interface ReservacionDAO {
    public Reservacion obtenerPorId(Integer id);

    public Reservacion agregar(Reservacion reservacion);

    public Cupo obtenerDisponiblidadCupo(Date fechaIngreso,
                            Date fechaSalida,
                            Optional<Integer> categoriaId,
                            Optional<Integer> offset,
                            Optional<Integer> limit);

    public void agregarHuesped(Huesped huesped);

    public Optional<Huesped>  obtenerHuespedPorEmail(String email);
}
