package ni.edu.ucem.webapi.dao;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.springframework.security.access.prepost.PreAuthorize;

import ni.edu.ucem.webapi.modelo.CategoriaCuarto;

public interface CategoriaCuartoDAO 
{
    @PreAuthorize("hasRole('ADMIN')")
    public CategoriaCuarto obtenerPorId(int pId, Optional<List<String>> fields);

    public List<CategoriaCuarto> obtenerTodos(Optional<List<String>> fields,
                                              Optional<String> sort,
                                              Optional<String> sortOrder,
                                              Optional<Integer> offset,
                                              Optional<Integer> limit);

    public void agregar(final CategoriaCuarto pCategoriaCuarto);

    public void guardar(final CategoriaCuarto pCategoriaCuarto);

    public void eliminar(final int pId);

    public List<CategoriaCuarto> searchCategoriaCuarto(
                                                Optional<String> nombre,
                                                Optional<String> descripcion,
                                                Optional<BigDecimal> precio,
                                                Optional<List<String>> fields,
                                                Optional<String> sort,
                                                Optional<String> sortOrder,
                                                Optional<Integer> offset,
                                                Optional<Integer> limit);

    public int contarTodos();

    public int contarSearchCategoriaCuarto(Optional<String> nombre,
                                           Optional<String> descripcion,
                                           Optional<BigDecimal> precio);

}
