package ni.edu.ucem.webapi.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import ni.edu.ucem.webapi.modelo.CategoriaCuarto;
import ni.edu.ucem.webapi.modelo.Cuarto;
import ni.edu.ucem.webapi.modelo.Filtro;
import ni.edu.ucem.webapi.modelo.Pagina;

public interface InventarioService 
{
    public void agregarCategoriaCuarto(final CategoriaCuarto pCategoriaCuarto);

    public void guardarCategoriaCuarto(final CategoriaCuarto pCategoriaCuarto);

    public void eliminarCategoriaCuarto(int id);
    
    public CategoriaCuarto obtenerCategoriaCuarto(
                                                 int id, 
                                                 Optional<List<String>> fields);

    public Pagina<CategoriaCuarto> obtenerTodosCategoriaCuartos(
                                                 Optional<List<String>> fields,
                                                 Optional<String> sort,
                                                 Optional<String> sortOrder,
                                                 Optional<Integer> offset,
                                                 Optional<Integer> limit);

    public void agregarCuarto(final Cuarto pCuarto);

    public void guardarCuarto(final Cuarto pCuarto);

    public void eliminarCuarto(final int pCuarto);

    public Cuarto obtenerCuarto(final int pCuarto);

    public Pagina<Cuarto> obtenerTodosCuarto(final Filtro paginacion);

    public Pagina<Cuarto> obtenerTodosCuartoEnCategoria(final int pCategoriaCuarto, final Filtro paginacion);

    public void validarFieldsCategoria(List<String> fields);

    public Pagina<CategoriaCuarto> searchCategoriaCuarto(
                                            Optional<String> nombre,
                                            Optional<String> descripcion,
                                            Optional<BigDecimal> precio,
                                            Optional<List<String>> fields,
                                            Optional<String> sort,
                                            Optional<String> sortOrder,
                                            Optional<Integer> offset,
                                            Optional<Integer> limit);

    public void validarSortOrder(String sortOrder);
}
