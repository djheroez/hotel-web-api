package ni.edu.ucem.webapi.modelo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Pagina<T>
{
    private List<T> data;
    private int count;
    private Integer offset;
    private Integer limit;
    
    public Pagina(){
        
    }

    public Pagina(final List<T> data, final int count, 
                  final Integer offset,
                  final Integer limit)
    {
        this.data = data;
        this.count = count;
        this.offset = offset;
        this.limit = limit;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }
}
