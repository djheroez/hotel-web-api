package ni.edu.ucem.webapi.modelo;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class CategoriaCuarto 
{
    private Integer id;
    private String nombre;
    private String descripcion;
    private BigDecimal precio;
    
    public CategoriaCuarto()
    {
    }
    
    public CategoriaCuarto(final Integer id, final String nombre, 
            final String descripcion,final BigDecimal precio) 
    {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.precio = precio;
    }
    
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public BigDecimal getPrecio() {
        return precio;
    }
    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    public static List<String> findNonExistentFields(final List<String> fields){
        List<String> fieldNames = CategoriaCuarto.getDeclaredFieldNames(); 
        return fields.stream()
                     .filter(field -> !fieldNames.contains(field))
                     .collect(Collectors.toList());
    }

    public static List<String> sanitizedFields(final List<String> fields){
        List<String> fieldNames = CategoriaCuarto.getDeclaredFieldNames();

        return fieldNames.stream()
                  .filter(field -> fields.contains(field))
                  .collect(Collectors.toList());
    }

    public static String getSelectFields(final Optional<List<String>> fields){
        String selectFields = "*";

        if(fields.isPresent())
        {
            List<String> sanitizedFields = CategoriaCuarto
                                                .sanitizedFields(fields.get());
            if(!CollectionUtils.isEmpty(sanitizedFields)){
                selectFields = sanitizedFields.stream()
                                              .collect(Collectors.joining(","));
            }
        }
        return selectFields;
    }
    
    private static List<String> getDeclaredFieldNames(){
        Field[] declaredFields = CategoriaCuarto.class.getDeclaredFields();
        return Stream.of(declaredFields)
                     .map(field -> field.getName())
                     .collect(Collectors.toList());
    }
}