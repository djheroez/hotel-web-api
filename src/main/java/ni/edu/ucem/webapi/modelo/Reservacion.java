package ni.edu.ucem.webapi.modelo;

import java.util.Date;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Reservacion {
    private Integer id;

    
    @NotNull
    @JsonProperty("desde")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone="CST")

    private Date fechaDesde;

    @NotNull
    @JsonProperty("hasta")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone="CST")
    private Date fechaHasta;

    @NotNull
    private Cuarto cuarto;

    @NotNull
    private Huesped huesped;

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Date getFechaDesde() {
        return fechaDesde;
    }
    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }
    public Date getFechaHasta() {
        return fechaHasta;
    }
    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }
    public Cuarto getCuarto() {
        return cuarto;
    }
    public void setCuarto(Cuarto cuarto) {
        this.cuarto = cuarto;
    }
    public Huesped getHuesped() {
        return huesped;
    }
    public void setHuesped(Huesped huesped) {
        this.huesped = huesped;
    }

}
