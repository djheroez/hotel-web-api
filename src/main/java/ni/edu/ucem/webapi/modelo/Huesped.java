package ni.edu.ucem.webapi.modelo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

public class Huesped {
    private Integer id;

    @NotNull
    @NotEmpty(message = "El nombre es requerido.")
    private String nombre;

    @NotNull
    @NotEmpty(message = "El email es requerido.")
    @Email(message = "El email debe ser válido.")
    private String email;

    @NotNull
    @NotEmpty(message = "El teléfono es requerido.")
    @Pattern(regexp = "^[\\d ]+$")
    private String telefono;

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getTelefono() {
        return telefono;
    }
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
}