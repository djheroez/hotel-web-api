package ni.edu.ucem.webapi.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import ni.edu.ucem.webapi.modelo.Cuarto;
import ni.edu.ucem.webapi.modelo.Huesped;
import ni.edu.ucem.webapi.modelo.Reservacion;

public class ReservacionRowMapper implements RowMapper<Reservacion>{

    @Override
    public Reservacion mapRow(ResultSet rs, int rowNum) throws SQLException {
        Cuarto cuarto = new Cuarto();
        cuarto.setId(rs.getInt("cuarto_id"));
        cuarto.setDescripcion(rs.getString("cuarto_descripcion"));
        cuarto.setCategoria(rs.getInt("cuarto_categoria"));
        cuarto.setModificado(rs.getDate("cuarto_modificado"));
        cuarto.setNumero(rs.getShort("cuarto_numero"));
        
        Huesped huesped = new Huesped();
        huesped.setId(rs.getInt("huesped_id"));
        huesped.setNombre(rs.getString("huesped_nombre"));
        huesped.setTelefono(rs.getString("huesped_telefono"));
        huesped.setEmail(rs.getString("huesped_email"));

        Reservacion reservacion = new Reservacion();
        reservacion.setId(rs.getInt("reservacion_id"));
        reservacion.setFechaDesde(rs.getDate("reservacion_desde"));
        reservacion.setFechaHasta(rs.getDate("reservacion_hasta"));
        reservacion.setCuarto(cuarto);
        reservacion.setHuesped(huesped);
        
        return reservacion;
    }
}