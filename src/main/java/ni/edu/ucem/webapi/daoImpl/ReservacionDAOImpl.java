package ni.edu.ucem.webapi.daoImpl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import ni.edu.ucem.webapi.dao.ReservacionDAO;
import ni.edu.ucem.webapi.mapper.ReservacionRowMapper;
import ni.edu.ucem.webapi.modelo.Cuarto;
import ni.edu.ucem.webapi.modelo.Cupo;
import ni.edu.ucem.webapi.modelo.Huesped;
import ni.edu.ucem.webapi.modelo.Reservacion;

@Repository
public class ReservacionDAOImpl implements ReservacionDAO {

    private final JdbcTemplate jdbcTemplate;
    private final NamedParameterJdbcTemplate namedJdbcTemplate;
    
    @Autowired
    public ReservacionDAOImpl(final JdbcTemplate jdbcTemplate)
    {
        this.jdbcTemplate = jdbcTemplate;
        this.namedJdbcTemplate = new NamedParameterJdbcTemplate(
                                    this.jdbcTemplate.getDataSource());
    }

    @Override
    public Reservacion obtenerPorId(final Integer id) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT ");
        sql.append("reservacion.id as reservacion_id, ");
        sql.append("reservacion.desde as reservacion_desde, ");
        sql.append("reservacion.hasta as reservacion_hasta, ");
        sql.append("reservacion.cuarto, ");
        sql.append("reservacion.huesped, ");
        sql.append("cuarto.id as cuarto_id, ");
        sql.append("cuarto.descripcion as cuarto_descripcion, ");
        sql.append("cuarto.categoria as cuarto_categoria, ");
        sql.append("cuarto.modificado as cuarto_modificado, ");
        sql.append("cuarto.numero as cuarto_numero, ");
        sql.append("huesped.id as huesped_id, ");
        sql.append("huesped.nombre as huesped_nombre, ");
        sql.append("huesped.email as huesped_email, ");
        sql.append("huesped.telefono as huesped_telefono ");
        sql.append("FROM reservacion ");
        sql.append("INNER JOIN cuarto ON ");
        sql.append("reservacion.cuarto = cuarto.id ");
        sql.append("INNER JOIN huesped ON ");
        sql.append("reservacion.huesped = huesped.id ");
        sql.append("WHERE reservacion.id = :id");

        Map<String, Object> parametros = new HashMap<>();
        parametros.put("id", id);

        return this.namedJdbcTemplate.queryForObject(
                            sql.toString(), 
                            parametros,
                            new ReservacionRowMapper());
    }

    @Override
    public Reservacion agregar(final Reservacion reservacion) {
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO reservacion (desde, hasta,cuarto,huesped)");
        sql.append("VALUES(:desde, :hasta, :cuarto, :huesped)");

        Map<String, Object> parametros = new HashMap<>();
        parametros.put("desde", reservacion.getFechaDesde());
        parametros.put("hasta", reservacion.getFechaHasta());
        parametros.put("cuarto", reservacion.getCuarto().getId());
        parametros.put("huesped", reservacion.getHuesped().getId());
        
        this.namedJdbcTemplate.update(sql.toString(), parametros);

        return reservacion;
    }

    @Override
    public void agregarHuesped(final Huesped huesped){
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO huesped(nombre, email, telefono) ");
        sql.append("VALUES(:nombre, :email, :telefono)");

        Map<String, Object> parametros = new HashMap<>();
        parametros.put("nombre", huesped.getNombre());
        parametros.put("email", huesped.getEmail());
        parametros.put("telefono", huesped.getTelefono());

        this.namedJdbcTemplate.update(sql.toString(), parametros);
    }

    @Override
    public Optional<Huesped> obtenerHuespedPorEmail(final String email){
        String sql = "SELECT * FROM huesped WHERE email = :email";
        Map<String, Object> parametros = new HashMap<>();
        parametros.put("email", email);
        Optional<Huesped> huesped = Optional.empty();
        try{
            huesped = Optional.ofNullable(
                            this.namedJdbcTemplate
                                .queryForObject(
                                       sql.toString(),
                                       parametros,
                                       new BeanPropertyRowMapper<Huesped>(Huesped.class)));
        } catch(EmptyResultDataAccessException e){
            
        }
        return huesped;
    }

    @Override
    public Cupo obtenerDisponiblidadCupo(final Date fechaIngreso,
                                         final Date fechaSalida,
                                         final Optional<Integer> categoriaId,
                                         final Optional<Integer> offset,
                                         final Optional<Integer> limit) {

        Map<String, Object> parametros = new HashMap<>();
        parametros.put("fechaIngreso", fechaIngreso);
        parametros.put("fechaSalida", fechaSalida);

        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM cuarto ");
        sql.append("WHERE id NOT IN ( ");
        sql.append(" SELECT cuarto FROM reservacion");
        sql.append(" WHERE (desde <= :fechaIngreso OR hasta <= :fechaIngreso)");
        sql.append(" OR (desde <= :fechaSalida OR hasta <= :fechaSalida)");
        sql.append(")");

        categoriaId.ifPresent(value ->{
            sql.append(" ");
            sql.append("AND categoria = :categoriaId");
            parametros.put("categoriaId", categoriaId);
        });

        List<Cuarto> cuartosDisponibles = this.namedJdbcTemplate.query(
                                            sql.toString(),
                                            parametros,
                                            new BeanPropertyRowMapper<Cuarto>(Cuarto.class));

        return new Cupo(fechaIngreso, fechaSalida, cuartosDisponibles);
    }

}