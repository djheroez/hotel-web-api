package ni.edu.ucem.webapi.daoImpl;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import ni.edu.ucem.webapi.dao.CategoriaCuartoDAO;
import ni.edu.ucem.webapi.modelo.CategoriaCuarto;

@Repository
public class CategoriaCuartoDAOImpl implements CategoriaCuartoDAO 
{
    private final JdbcTemplate jdbcTemplate;
    
    @Autowired
    public CategoriaCuartoDAOImpl(final JdbcTemplate jdbcTemplate)
    {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public CategoriaCuarto obtenerPorId(final int pId, 
                                        final Optional<List<String>> fields) 
    {
        String selectFields = CategoriaCuarto.getSelectFields(fields);
        final String sql = "select " + selectFields + 
                           " from categoria_cuarto where id = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{pId}, 
                new BeanPropertyRowMapper<CategoriaCuarto>(CategoriaCuarto.class));
    }

    @Override
    public int contarTodos(){
        String sql = "select COUNT(*) from categoria_cuarto";
        return this.jdbcTemplate.queryForObject(sql, Integer.class).intValue();
    }

    @Override
    public List<CategoriaCuarto> obtenerTodos(final Optional<List<String>> fields,
                                              final Optional<String> sort,
                                              final Optional<String> sortOrder,
                                              final Optional<Integer> offset,
                                              final Optional<Integer> limit) 
    {
        String selectFields = CategoriaCuarto.getSelectFields(fields);
        StringBuilder sql = new StringBuilder();
        sql.append("select");
        sql.append(selectFields);
        sql.append(" from categoria_cuarto ");

        sort.ifPresent( sortField -> {
            String orderFieldSanitized = CategoriaCuarto.getSelectFields(
                                             Optional.of(
                                                 Arrays.asList(new String[]{sortField})
                                             ));
            sql.append("ORDER BY ");
            sql.append(orderFieldSanitized);
            sql.append(" ");
            sql.append(sortOrder.orElse("ASC"));
        });

        Map<String, Object> parametros = new HashMap<>(); 

        offset.ifPresent(offsetValue -> {
            parametros.put("offset", offsetValue);
            sql.append(" OFFSET :offset ");
        });

        limit.ifPresent(limitValue -> {
            parametros.put("limit", limitValue);
            sql.append(" LIMIT :limit ");
        });

        NamedParameterJdbcTemplate namedJdbcTemplate =
                            new NamedParameterJdbcTemplate(
                                             this.jdbcTemplate.getDataSource());

        return namedJdbcTemplate.query(
                sql.toString(),
                parametros,
                new BeanPropertyRowMapper<CategoriaCuarto>(CategoriaCuarto.class));
    }

    @Override
    public int contarSearchCategoriaCuarto(
                                        final Optional<String> nombre,
                                        final Optional<String> descripcion,
                                        final Optional<BigDecimal> precio)
    {
        StringBuilder sql = new StringBuilder(); 
        sql.append("select COUNT(*) from categoria_cuarto");

        Map<String, Object> parametros = new HashMap<>(); 
        if(nombre.isPresent() || descripcion.isPresent() || precio.isPresent()){
            sql.append(" WHERE ");
        }
        if(nombre.isPresent()){
          sql.append(" nombre = :nombre");
          parametros.put("nombre", nombre.get());
          if(descripcion.isPresent()){
              sql.append(" AND descripcion like lower(:descripcion)");
              parametros.put("descripcion", "%" + descripcion.get() + "%");
          }
          if(precio.isPresent()){
              sql.append(" AND precio = :precio");
              parametros.put("precio", precio.get());
          }
        } else if(descripcion.isPresent()){
            sql.append(" descripcion like lower(:descripcion)");
            parametros.put("descripcion", "%" + descripcion.get() + "%");
            if(precio.isPresent()){
                sql.append(" AND precio = :precio");
                parametros.put("precio", precio.get());
            }
        } else if(precio.isPresent()){
            sql.append(" precio = :precio");
            parametros.put("precio", precio.get());
        }
        
        NamedParameterJdbcTemplate namedJdbcTemplate =
                new NamedParameterJdbcTemplate(
                    this.jdbcTemplate.getDataSource());

        return namedJdbcTemplate.queryForObject(
                                        sql.toString(),
                                        parametros, 
                                        Integer.class).intValue();
    }

    @Override
    public List<CategoriaCuarto> searchCategoriaCuarto(
                                            final Optional<String> nombre,
                                            final Optional<String> descripcion,
                                            final Optional<BigDecimal> precio,
                                            final Optional<List<String>> fields,
                                            final Optional<String> sort,
                                            final Optional<String> sortOrder,
                                            final Optional<Integer> offset,
                                            final Optional<Integer> limit)
    {
        String selectFields = CategoriaCuarto.getSelectFields(fields);
        StringBuilder sql = new StringBuilder(); 
        sql.append("select "+ selectFields + " from categoria_cuarto");

        Map<String, Object> parametros = new HashMap<>(); 
        if(nombre.isPresent() || descripcion.isPresent() || precio.isPresent()){
            sql.append(" WHERE ");
        }
        if(nombre.isPresent()){
          sql.append(" nombre = :nombre");
          parametros.put("nombre", nombre.get());
          if(descripcion.isPresent()){
              sql.append(" AND descripcion like lower(:descripcion)");
              parametros.put("descripcion", "%" + descripcion.get() + "%");
          }
          if(precio.isPresent()){
              sql.append(" AND precio = :precio");
              parametros.put("precio", precio.get());
          }
        } else if(descripcion.isPresent()){
            sql.append(" descripcion like lower(:descripcion)");
            parametros.put("descripcion", "%" + descripcion.get() + "%");
            if(precio.isPresent()){
                sql.append(" AND precio = :precio");
                parametros.put("precio", precio.get());
            }
        } else if(precio.isPresent()){
            sql.append(" precio = :precio");
            parametros.put("precio", precio.get());
        }

        sort.ifPresent( sortField -> {
            String orderFieldSanitized = CategoriaCuarto.getSelectFields(
                                             Optional.of(
                                                 Arrays.asList(new String[]{sortField})
                                             ));
            sql.append(" ORDER BY ");
            sql.append(orderFieldSanitized);
            sql.append(" ");
            sql.append(sortOrder.orElse("ASC"));
        });

        offset.ifPresent(offsetValue -> {
            parametros.put("offset", offsetValue);
            sql.append(" OFFSET :offset ");
        });

        limit.ifPresent(limitValue -> {
            parametros.put("limit", limitValue);
            sql.append(" LIMIT :limit ");
        });

        NamedParameterJdbcTemplate namedJdbcTemplate =
                                        new NamedParameterJdbcTemplate(
                                            this.jdbcTemplate.getDataSource());
        return namedJdbcTemplate.query(
                sql.toString(),
                parametros,
                new BeanPropertyRowMapper<CategoriaCuarto>(CategoriaCuarto.class));
    }

    @Override
    public void agregar(final CategoriaCuarto pCategoriaCuarto) 
    {
        final String sql = new StringBuilder()
                .append("INSERT INTO categoria_cuarto")
                .append(" ")
                .append("(nombre, descripcion, precio)")
                .append(" ")
                .append("VALUES(?,?,?)")
                .toString();
        final Object[] parametros = new Object[3];
        parametros[0] = pCategoriaCuarto.getNombre();
        parametros[1] = pCategoriaCuarto.getDescripcion();
        parametros[2] = pCategoriaCuarto.getPrecio();
        this.jdbcTemplate.update(sql,parametros);
    }

    @Override
    public void guardar(final CategoriaCuarto pCategoriaCuarto) 
    {
        final String sql = new StringBuilder()
                .append("UPDATE categoria_cuarto")
                .append(" ")
                .append("SET nombre = ?, descripcion = ?")
                .append(",precio = ?")
                .append(" ")
                .append("WHERE id = ?")
                .toString();
        final Object[] parametros = new Object[4];
        parametros[0] = pCategoriaCuarto.getNombre();
        parametros[1] = pCategoriaCuarto.getDescripcion();
        parametros[2] = pCategoriaCuarto.getPrecio();
        parametros[3] = pCategoriaCuarto.getId();
        this.jdbcTemplate.update(sql,parametros);
    }

    @Override
    public void eliminar(final int pId) 
    {
        final String sql = "delete from categoria_cuarto where id = ?";
        this.jdbcTemplate.update(sql, new Object[]{pId});
    }
}