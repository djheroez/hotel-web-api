package ni.edu.ucem.webapi.web.inventario;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ni.edu.ucem.webapi.core.ApiResponse;
import ni.edu.ucem.webapi.core.ApiResponse.Status;
import ni.edu.ucem.webapi.core.ListApiResponse;
import ni.edu.ucem.webapi.modelo.CategoriaCuarto;
import ni.edu.ucem.webapi.modelo.Pagina;
import ni.edu.ucem.webapi.service.InventarioService;

@RestController
@RequestMapping("/v1/inventario/categorias")
public class CategoriaResource {
    private final InventarioService inventarioService;

    @Autowired
    public CategoriaResource(final InventarioService inventarioService)
    {
        this.inventarioService = inventarioService;
    }

    @GetMapping(produces="application/json")
    public ListApiResponse<CategoriaCuarto> obtenerCategoriasCuarto(
                                               final Optional<String[]> fields,
                                               final Optional<String> sort,
                                               final Optional<String> sortOrder,
                                               final Optional<Integer> offset,
                                               final Optional<Integer> limit){

        Optional<List<String>> listOfFields = fields.map(fs -> Arrays.asList(fs));

        listOfFields.ifPresent(this.inventarioService::validarFieldsCategoria);

        sort.ifPresent(sortField -> {
            this.inventarioService
                .validarFieldsCategoria(Arrays.asList(new String[]{sortField}));
            sortOrder.ifPresent(this.inventarioService::validarSortOrder);
        });

        Pagina<CategoriaCuarto> pagina = this.inventarioService
                                                     .obtenerTodosCategoriaCuartos(
                                                             listOfFields,
                                                             sort,
                                                             sortOrder,
                                                             offset,
                                                             limit);

        return new ListApiResponse<CategoriaCuarto>(Status.OK, pagina);
    }

    @GetMapping(produces="application/json", value="/search")
    public ListApiResponse<CategoriaCuarto> searchCategoriaCuarto(
                                          final Optional<String> nombre,
                                          final Optional<String> descripcion,
                                          final Optional<BigDecimal> precio,
                                          final Optional<String[]> fields,
                                          final Optional<String> sort,
                                          final Optional<String> sortOrder,
                                          final Optional<Integer> offset,
                                          final Optional<Integer> limit){

        Optional<List<String>> listOfFields = fields.map(fs -> Arrays.asList(fs));

        listOfFields.ifPresent(this.inventarioService::validarFieldsCategoria);

        sort.ifPresent(sortField -> {
            this.inventarioService
                .validarFieldsCategoria(Arrays.asList(new String[]{sortField}));
            sortOrder.ifPresent(this.inventarioService::validarSortOrder);
        });

        Pagina<CategoriaCuarto> pagina = this.inventarioService
                                             .searchCategoriaCuarto(
                                                  nombre,
                                                  descripcion,
                                                  precio,
                                                  listOfFields,
                                                  sort,
                                                  sortOrder,
                                                  offset,
                                                  limit);

        return new ListApiResponse<CategoriaCuarto>(Status.OK, pagina);
    }

    @GetMapping(value = "/{id}", produces="application/json")
    public ApiResponse obtenerCategoriaCuarto(
                                        @PathVariable("id") final int id,
                                        Optional<String[]> fields){

        Optional<List<String>> listOfFields = fields.map(fs -> Arrays.asList(fs));

        listOfFields.ifPresent(this.inventarioService::validarFieldsCategoria);

        final CategoriaCuarto categoria = this.inventarioService
                                              .obtenerCategoriaCuarto(
                                                                  id,
                                                                  listOfFields);
        return new ApiResponse(Status.OK, categoria);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public ApiResponse guardarCategoriaCuarto(
            @RequestBody @Valid final CategoriaCuarto categoriaCuarto,
            BindingResult result){
        if(result.hasErrors())
        {
            throw new IllegalArgumentException(result.getFieldError().getDefaultMessage());
        }
        this.inventarioService.agregarCategoriaCuarto(categoriaCuarto);
        return new ApiResponse(Status.OK, categoriaCuarto);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping(value = "/{id}", produces="application/json")
    public ApiResponse guardarCategoriaCuarto(
            @PathVariable("id") final int id,
            @RequestBody @Valid final CategoriaCuarto categoriaCuarto,
            BindingResult result){

        if(result.hasErrors())
        {
            throw new IllegalArgumentException(result.getFieldError().getDefaultMessage());
        }

        final CategoriaCuarto categoriaCuartoActualizado = new CategoriaCuarto(
                                                        id,
                                                        categoriaCuarto.getNombre(),
                                                        categoriaCuarto.getDescripcion(),
                                                        categoriaCuarto.getPrecio());
        this.inventarioService.guardarCategoriaCuarto(categoriaCuartoActualizado);
        return new ApiResponse(Status.OK, categoriaCuartoActualizado);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping(value = "/{id}", produces="application/json")
    public ApiResponse eliminarCategoriaCuarto(
                                            @PathVariable("id") final int id){
       final CategoriaCuarto categoriaCuarto = this.inventarioService
                                                   .obtenerCategoriaCuarto(
                                                              id,
                                                              Optional.empty());
       this.inventarioService.eliminarCategoriaCuarto(categoriaCuarto.getId());
       return new ApiResponse(Status.OK, null);
    }

}
