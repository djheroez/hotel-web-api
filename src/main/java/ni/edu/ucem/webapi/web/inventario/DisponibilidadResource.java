package ni.edu.ucem.webapi.web.inventario;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ni.edu.ucem.webapi.modelo.Cupo;
import ni.edu.ucem.webapi.service.ReservacionService;

@RestController
@RequestMapping("/v1/disponiblidad")
public class DisponibilidadResource {
    
    private final ReservacionService reservacionService;

    @Autowired
    public DisponibilidadResource(final ReservacionService reservacionService){
        this.reservacionService = reservacionService;
    }
    
    @GetMapping("/cupos")
    public Cupo obtenerCupo(@RequestParam(required = true)
                            @DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                            final Date fechaIngreso,
                            @RequestParam(required = true)
                            @DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                            final Date fechaSalida,
                            Optional<Integer> categoriaId,
                            Optional<Integer> offset,
                            Optional<Integer> limit){
        return this.reservacionService
                   .obtenerDisponiblidadCupo(fechaIngreso,
                                             fechaSalida,
                                             categoriaId,
                                             offset,
                                             limit);
    }
}
