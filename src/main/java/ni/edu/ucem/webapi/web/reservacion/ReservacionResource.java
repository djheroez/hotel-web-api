package ni.edu.ucem.webapi.web.reservacion;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ni.edu.ucem.webapi.modelo.Reservacion;
import ni.edu.ucem.webapi.service.ReservacionService;

@RestController
@RequestMapping("/v1/reservaciones")
public class ReservacionResource {
    
    private final ReservacionService reservacionService;

    @Autowired
    public ReservacionResource(final ReservacionService reservacionService){
        this.reservacionService = reservacionService;
    }

    @GetMapping("/{id}")
    public Reservacion obtenerPorId(@PathVariable("id") final Integer id){
        return this.reservacionService.obtenerPorId(id);
    }

    @PostMapping
    public Reservacion agregarReservacion(@Valid @RequestBody final Reservacion reservacion){
        return this.reservacionService.agregar(reservacion);
        
    }
}
